#!python
#cython: language_level=3

cdef double L3(double x,double y, double z):
    return (x**2+y**2+z**2)**(1.5)

import numpy as np
cimport numpy as np
cimport cython
@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def CGravity(np.ndarray[double, ndim=1] m,np.ndarray[double, ndim=2] P0,np.ndarray[double, ndim=2] V0, double T,double DELTA,long l):
    cdef long n = int(T/DELTA)
    cdef double G = 6.673e-11
    cdef int N = len(P0)
    l = np.minimum(l,n)

    cdef np.ndarray[double, ndim=2] Pt = P0
    cdef np.ndarray[double, ndim=2] Vt = V0

    cdef double Rx, Ry, Rz, Sx, Sy, Sz, L
    cdef long k
    cdef int i, j
    cdef int D = n//l
    
    cdef np.ndarray[double, ndim=3] P = np.zeros((N,3,n//D+1),dtype=np.double)
    cdef np.ndarray[double, ndim=3] V = np.zeros((N,3,n//D+1),dtype=np.double)
    P[:,:,0] = P0   
    V[:,:,0] = V0
    for k in range(n):
        for j in range(N):
            Sx=Sy=Sz=0.0
            for i in range(N):
                Rx = Pt[i,0]-Pt[j,0]
                Ry = Pt[i,1]-Pt[j,1]
                Rz = Pt[i,2]-Pt[j,2]
                                
                L = L3(Rx,Ry,Rz) + (i==j)
                Sx += m[i]*Rx/L
                Sy += m[i]*Ry/L
                Sz += m[i]*Rz/L
                
            
            Pt[j,0] +=  DELTA*Vt[j,0] + DELTA*DELTA*G*Sx/2 
            Pt[j,1] +=  DELTA*Vt[j,1] + DELTA*DELTA*G*Sy/2
            Pt[j,2] +=  DELTA*Vt[j,2] + DELTA*DELTA*G*Sz/2 
            
            Vt[j,0] +=  DELTA*G*Sx 
            Vt[j,1] +=  DELTA*G*Sy
            Vt[j,2] +=  DELTA*G*Sz
            
            if (k+1)%D==0:
                P[j,0,(k+1)//D] = Pt[j,0]
                P[j,1,(k+1)//D] = Pt[j,1]
                P[j,2,(k+1)//D] = Pt[j,2]
                V[j,0,(k+1)//D] = Vt[j,0]
                V[j,1,(k+1)//D] = Vt[j,1]
                V[j,2,(k+1)//D] = Vt[j,2]
    return P,V

import numpy as np
cimport numpy as np
cimport cython
@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def CGravity2D(np.ndarray[double, ndim=1] m,np.ndarray[double, ndim=2] P0,np.ndarray[double, ndim=2] V0, double T,double DELTA,long l):
    cdef long n = int(T/DELTA)
    cdef double DG = DELTA*(6.673e-11)
    cdef int N = len(P0)
    l = np.minimum(l,n)

    cdef np.ndarray[double, ndim=2] Pt = P0
    cdef np.ndarray[double, ndim=2] Vt = V0

    cdef double Rjx, Rjy, Sx, Sy, Lj3
    cdef long k
    cdef int i, j
    cdef int D = n//l
    
    cdef np.ndarray[double, ndim=3] P = np.zeros((N,2,n//D+1),dtype=np.double)
    cdef np.ndarray[double, ndim=3] V = np.zeros((N,2,n//D+1),dtype=np.double)
    P[:,:,0] = P0   
    V[:,:,0] = V0
    for k in range(n):
        for j in range(N):
            Sx=Sy=0.0
            for i in range(N):
                Rjx = Pt[i,0]-Pt[j,0]
                Rjy = Pt[i,1]-Pt[j,1]
                Lj3 = (Rjx**2+Rjy**2)**(1.5) + (i==j)
                Sx += m[i]*Rjx/Lj3
                Sy += m[i]*Rjy/Lj3
            
            Pt[j,0] +=  DELTA*Vt[j,0]
            Pt[j,1] +=  DELTA*Vt[j,1] 
            
            Vt[j,0] +=  DG*Sx 
            Vt[j,1] +=  DG*Sy
            
            if (k+1)%D==0:
                P[j,0,(k+1)//D] = Pt[j,0]
                P[j,1,(k+1)//D] = Pt[j,1]
                V[j,0,(k+1)//D] = Vt[j,0]
                V[j,1,(k+1)//D] = Vt[j,1]
    return P,V

@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function
def CGravity2D(np.ndarray[double, ndim=1] m,np.ndarray[double, ndim=2] P0,np.ndarray[double, ndim=2] V0, double T,double DELTA,long l):
    cdef long n = int(T/DELTA)
    cdef double DG = DELTA*(6.673e-11)
    cdef int N = len(P0)
    l = np.minimum(l,n)

    cdef np.ndarray[double, ndim=2] Pt = P0
    cdef np.ndarray[double, ndim=2] Vt = V0

    cdef double Rjx, Rjy, Sx, Sy, Lj3
    cdef long k
    cdef int i, j
    cdef int D = n//l
    
    cdef np.ndarray[double, ndim=3] P = np.zeros((N,2,n//D+1),dtype=np.double)
    cdef np.ndarray[double, ndim=3] V = np.zeros((N,2,n//D+1),dtype=np.double)
    P[:,:,0] = P0   
    V[:,:,0] = V0
    for k in range(n):
        for j in range(N):
            Sx=Sy=0.0
            for i in range(N):
                Rjx = Pt[i,0]-Pt[j,0]
                Rjy = Pt[i,1]-Pt[j,1]
                Lj3 = (Rjx**2+Rjy**2)**(1.5) + (i==j)
                Sx += m[i]*Rjx/Lj3
                Sy += m[i]*Rjy/Lj3
            
            Pt[j,0] +=  DELTA*Vt[j,0]
            Pt[j,1] +=  DELTA*Vt[j,1] 
            
            Vt[j,0] +=  DG*Sx 
            Vt[j,1] +=  DG*Sy
            
            if (k+1)%D==0:
                P[j,0,(k+1)//D] = Pt[j,0]
                P[j,1,(k+1)//D] = Pt[j,1]
                V[j,0,(k+1)//D] = Vt[j,0]
                V[j,1,(k+1)//D] = Vt[j,1]
    return P,V

