import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
import mpl_toolkits.mplot3d.axes3d as p3
import pyximport
pyximport.install(setup_args={"include_dirs":np.get_include()},reload_support=True)
import CGravity

def Gravity(m,P0,V0,T,DELTA,l=1200):
    m  = np.array(m, dtype=np.double)
    P0 = np.array(P0,dtype=np.double)
    V0 = np.array(V0,dtype=np.double)
    T  = np.double(T)
    DELTA = np.double(DELTA)
    if P0.shape[1]==2:
        PV = CGravity.CGravity2D(m,P0,V0,T,DELTA,l)
    if P0.shape[1]==3:
        PV = CGravity.CGravity(m,P0,V0,T,DELTA,l)
    return {'P':PV[0],'V':PV[1],'m':m,'T':T,'DELTA0':DELTA}


cols = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']

def PLOT3D(Gr, fps=60,end='inf'):
    if end != 'inf':
        II = int(end*Gr['P'].shape[2]/Gr['T'])
        Data = Gr['P'][:,:,:II]
    else:
        Data = Gr['P']
        end = Gr['T']
    sm = 1.5*np.log(np.abs(Gr['m']))
    sm1 = 4+sm-np.min(sm)
    pointsize = 5*sm1/np.min(sm1)
    SHAPE=Data.shape
    lD = len(Data)
    xmin = np.min(Data[:,0,:])
    xmax = np.max(Data[:,0,:])
    ymin = np.min(Data[:,1,:])
    ymax = np.max(Data[:,1,:])
    zmin = np.min(Data[:,2,:])
    zmax = np.max(Data[:,2,:])
    xl = xmax-xmin
    yl = ymax-ymin
    zl = zmax-zmin

    Ml = np.max([xl,yl,zl])
    if xl == 0: xl = 20*Ml
    if yl == 0: yl = 20*Ml
    if zl == 0: zl = 20*Ml

    #######
    fig, ax = plt.subplots()
    plt.figure(figsize=(10,10))
    ax = p3.Axes3D(fig)
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False
    ax.xaxis.pane.set_edgecolor('w')
    ax.yaxis.pane.set_edgecolor('w')
    ax.zaxis.pane.set_edgecolor('w')
    line = []
    for i in range(lD):
        line += ax.plot([],[],[],linewidth=1)
    for i in range(lD):
        line += ax.plot([],[],[],marker='o',color=cols[i%10],markersize=pointsize[i])
    for i in range(lD):
        line += ax.plot([],[],[],linestyle='--',linewidth=0.9,color=cols[i%10])
    for i in range(lD):
        line += ax.plot([],[],[],linestyle='--',linewidth=0.8,color=cols[i%10])


    time_text = ax.text(xmin-0.015*xl, ymax+0.01*yl,zmax+0.01*zl, '', fontsize=11,color=cols[0])
    dt = float((end/SHAPE[2])/(60*60*24))
    def update(num):
        for i in range(lD):
            line[i].set_data(Data[i][:2, :num])
            line[i].set_3d_properties(Data[i][2, :num])
        for i in range(lD,2*lD):
            line[i].set_data(Data[i-lD][:2, num])
            line[i].set_3d_properties(Data[i-lD][2, num])
        for i in range(2*lD,3*lD):
            line[i].set_data([[Data[i-2*lD][0, num],Data[i-2*lD][0, num]],[Data[i-2*lD][1, num],Data[i-2*lD][1, num]]])
            line[i].set_3d_properties([zmin-0.01*zl,Data[i-2*lD][2, num]])
        for i in range(3*lD,4*lD):
            line[i].set_data(Data[i-3*lD][:2, :num])
            line[i].set_3d_properties(zmin-0.01*zl)
        time_text.set_text('time = %.1f days' % (num*dt))
        return [line[i] for i in range(lD)]+[line[i+lD] for i in range(lD)]+[line[i+2*lD] for i in range(lD)]+[line[i+3*lD] for i in range(lD)] + [time_text]

    ax.set_xlim3d([xmin-0.01*xl, xmax+0.01*xl])
    ax.set_xlabel('X')

    ax.set_ylim3d([ymin-0.01*yl, ymax+0.01*yl])
    ax.set_ylabel('Y')

    ax.set_zlim3d([zmin-0.01*zl, zmax+0.01*zl])
    ax.set_zlabel('Z')
    #plt.gca().set_aspect('equal')
    ani = animation.FuncAnimation(fig, update, frames=SHAPE[2],interval=1000/fps, blit=True)
    plt.close()
    return ani


def PLOT2D(Gr, fps=60):
    Data = Gr['P']
    sm = 1.5*np.log(np.abs(Gr['m']))
    sm1 = 4+sm-np.min(sm)
    pointsize = 5*sm1/np.min(sm1)
    SHAPE=Data.shape
    lD = len(Data)
    xmin = np.min(Data[:,0,:])
    xmax = np.max(Data[:,0,:])
    ymin = np.min(Data[:,1,:])
    ymax = np.max(Data[:,1,:])

    xl = xmax-xmin
    yl = ymax-ymin


    Ml = np.max([xl,yl])
    if xl == 0: xl = 20*Ml
    if yl == 0: yl = 20*Ml

    #######
    fig, ax = plt.subplots()
    plt.figure(figsize=(12,12))
    line = []
    for i in range(lD):
        line += ax.plot([],[],linewidth=1)
    for i in range(lD):
        line += ax.plot([],[],marker='o',color=cols[i%10],markersize=pointsize[i])

    time_text = ax.text(xmin+0.02*xl, ymax-0.04*yl,'', fontsize=11,color=cols[0])
    dt = float((Gr['T']/SHAPE[2])/(60*60*24))
    def update(num):
        for i in range(lD):
            line[i].set_data(Data[i][:2, :num])
        for i in range(lD,2*lD):
            line[i].set_data(Data[i-lD][:2, num])

        time_text.set_text('time = %.1f days' % (num*dt))
        return [line[i] for i in range(lD)]+[line[i+lD] for i in range(lD)]+ [time_text]

    ax.set_xlim([xmin-0.01*xl, xmax+0.01*xl])
    ax.set_xlabel('X')

    ax.set_ylim([ymin-0.01*yl, ymax+0.01*yl])
    ax.set_ylabel('Y')

    plt.gca().set_aspect('equal')

    ani = animation.FuncAnimation(fig, update, frames=SHAPE[2],interval=1000/fps, blit=True);
    plt.close()
    return ani

def PLOT(Gr, fps=60,end='inf'):
    if Gr['V'].shape[1] == 2:
        return PLOT2D(Gr, fps=fps)
    else:
        return PLOT3D(Gr, fps=fps, end=end)
